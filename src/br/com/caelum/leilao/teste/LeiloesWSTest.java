package br.com.caelum.leilao.teste;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.path.xml.XmlPath;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import br.com.caelum.leilao.modelo.Leilao;
import br.com.caelum.leilao.modelo.Usuario;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class LeiloesWSTest {

    private Usuario mauricio;

    @Before
    public void setup() {
        mauricio = new Usuario(1L, "Mauricio Aniche", "mauricio.aniche@caelum.com.br");
    }


    @Test
    public void deveRetornarLeilaoPeloId() {
        JsonPath path = given()
                .header("Accept", "application/json")
                .queryParam("leilao.id", 1)
                .get("/leiloes/show")
                .andReturn().jsonPath();

        Leilao leilao = path.getObject("leilao", Leilao.class);

        Leilao leilaoEsperado = new Leilao(
                1L,
                "Geladeira",
                800.00,
                mauricio,
                false);

        assertEquals(leilaoEsperado, leilao);
    }


    @Test
    public void deveRetornarQuantidadeDeLeiloes() {
        XmlPath path = given()
                .header("Accept", "application/xml")
                .get("/leiloes/total")
                .andReturn().xmlPath();

        int total = path.getInt("int");

        assertEquals(4, total);
    }

    @Test
    public void deveAdicionarUmLeilao() {

        Leilao leilao = new Leilao(
                "PS 4",
                800.00,
                mauricio,
                false);

        XmlPath path = given()
                .header("Accept", "application/xml")
                .contentType("application/xml")
                .body(leilao)
                .expect()
                .statusCode(HttpStatus.SC_OK)
                .when()
                .post("/leiloes")
                .andReturn()
                .xmlPath();

        Leilao resposta = path.getObject("leilao", Leilao.class);

        assertEquals("PS 4", resposta.getNome());
        assertEquals("Mauricio Aniche", resposta.getUsuario().getNome());

        given()
                .contentType("application/xml").body(resposta)
                .expect().statusCode(HttpStatus.SC_OK)
                .when().delete("/leiloes/deletar")
                .andReturn().asString();
    }

}
